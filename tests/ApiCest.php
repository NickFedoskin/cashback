<?php
class ApiCest 
{
  protected $token = '587ad896-ec42-4144-9714-a9ef33b095db';

  protected function setAuth(ApiTester $I)
  {
    $I->haveHttpHeader('Authorization', $this->token);
    $I->haveHttpHeader('OS', 'ios');
    $I->haveHttpHeader('App-Version', '1.08');
  }

  public function testIsHomePageAccessible(ApiTester $I)
  {
    $I->sendGET('/');
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
  }
  public function testRegisterMethod(ApiTester $I)
  {
    $this->setAuth($I);
    $I->sendGET('/register');
    $I->seeResponseCodeIs(200);
  }
  public function testGetCatalogMethod(ApiTester $I)
  {
    $this->setAuth($I);
    $I->sendGET('/catalog');
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
  }
  public function testGetOffersForSection(ApiTester $I)
  {
    $this->setAuth($I);
    $I->sendGET('/catalog', ['Section'=>1]);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
  }

  public function testGetFullCatalog(ApiTester $I)
  {
    $this->setAuth($I);
    $I->sendGET('/catalog', ['full'=>'']);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
  }

  public function testGetProfile(ApiTester $I)
  {
    $this->setAuth($I);
    $I->sendGET('/profile');
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
  }

  public function testGetHistory(ApiTester $I)
  {
    $this->setAuth($I);
    $I->sendGET('/history');
    $I->seeResponseCodeIs(200);
    //$I->seeResponseIsJson();
  }

}