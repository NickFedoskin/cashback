<?php

use Phalcon\Mvc\View\Simple as View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Cache\Backend\Redis;
use Phalcon\Cache\Frontend\Data as FrontData;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * Sets the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    return $view;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->dbFreecom->adapter;
    $params = [
        'host'     => $config->dbFreecom->host,
        'username' => $config->dbFreecom->username,
        'password' => $config->dbFreecom->password,
        'dbname'   => $config->dbFreecom->dbname,
        'charset'  => $config->dbFreecom->charset
    ];

    if ($config->dbFreecom->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});

$di->setShared('redis', function() {
  $redisConfig = $this->getConfig()->redis;
  $frontCache = new FrontData(
    [
      "lifetime" => 172800,
    ]
  );
  return new Redis(
    $frontCache,
    [
      "host"       => $redisConfig->host,
      "port"       => $redisConfig->port,
      "persistent" => $redisConfig->persistent,
      "index"      => $redisConfig->index,
    ]
  );
});

$di->setShared('baseUrl', function () {
  return $this->getConfig()->APP->baseUrl;
});
$di->setShared('production', function () {
  return $this->getConfig()->APP->production;
});

