<?php
  defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ? : realpath(dirname(__FILE__) . '/../..'));
  defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

  $iniConfig = new \Phalcon\Config\Adapter\Ini(APP_PATH . '/config/config.ini');
  $config    = new \Phalcon\Config([
    'application' => [
      'modelsDir'     => APP_PATH . '/models/',
      'classesDir'    => APP_PATH . '/classes/',
      'migrationsDir' => APP_PATH . '/migrations/',
      'baseUri'       => '/api/',
    ],
  ]);
  $config->merge($iniConfig);
  return $config;
