<?php
namespace Classes\PAPI;

use Classes\Exceptions\PAPIException;
use Classes\PAPI\Responses\PapiResponse;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Di;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Интеграция с PAPI
 */
class PapiClient
{
    const QUEUE_NAME = 'REST_PAPI_REQUESTS_QUEUE';

    private $connection;
    private $channel;
    private $callback_queue;
    private $corr_id;

    private $response;

    public function __construct()
    {
        $config = Di::getDefault()->getConfig();
        $this->connection = new AMQPStreamConnection(
            $config->rabbit->host, $config->rabbit->port, $config->rabbit->user, $config->rabbit->password);
        $this->channel = $this->connection->channel();
        list($this->callback_queue, ,) = $this->channel->queue_declare(
            "", false, false, true, false);
        $this->channel->basic_consume(
            $this->callback_queue, '', false, false, false, false,
            array($this, 'on_response'));
    }

    public function on_response($rep)
    {
        if ($rep->get('correlation_id') == $this->corr_id) {
            $this->response = $rep->body;
        }
    }

    public function call($request, $params) : PapiResponse
    {
        /** @var Mysql $db */
        $db = Di::getDefault()->getShared('db');
        // Закрываем все коннекты, что бы они не висели, на случай долгого ожидания
        $db->close();
        $this->response = null;
        $this->corr_id = uniqid();

        $msg = new AMQPMessage(
            json_encode(['request' => $request, 'params' => $params]),
            array('correlation_id' => $this->corr_id,
                'reply_to' => $this->callback_queue)
        );
        $this->channel->basic_publish($msg, '', static::QUEUE_NAME);
        while (!$this->response) {
            try {
                $this->channel->wait(null, false, 45);// TODO: времнно. После тестов сделать 10
            } catch (AMQPTimeoutException $e) {
                $this->response = '';
                break;
            }
        }
        // Восстанавилваем соединение, на случай если оно понадобится
        $db->connect();
        $response = PapiResponse::parse($this->response);
        if ($response == null) {
            throw new PAPIException(PapiErrors::SERVICE_IS_UNAVAILABLE);
        }
        return $response;
    }

}