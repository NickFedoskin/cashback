<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 28.11.2017
 * Time: 17:46
 */

namespace Classes\PAPI;


class UserStatus
{
    private function __construct()
    {
    }

    const IN_PROGRESS = ['Code' => 0, 'Description' => 'IN_PROGRESS'];
    const REGISTERED = ['Code' => 1, 'Description' => 'REGISTERED'];
    const NOT_MEMBER = ['Code' => 2, 'Description' => 'NOT_MEMBER'];
    const CLOSED = ['Code' => 3, 'Description' => 'CLOSED'];
    const FROZEN = ['Code' => 4, 'Description' => 'FROZEN'];

    public static function parsePapiStatus($status)
    {
        switch ($status) {
            case "InProcessRegistration":
                $userStatus = static::IN_PROGRESS;
                break;
            case "Frozen":
                $userStatus = static::FROZEN;
                break;
            case "Closed":
                $userStatus = static::CLOSED;
                break;
            case "NotMemberFreeCom":
                $userStatus = static::NOT_MEMBER;
                break;
            case "MemberFreeCom":
            default:
                $userStatus = static::REGISTERED;
        }
        return $userStatus;
    }

}