<?php
declare(strict_types = 1);

namespace Classes\PAPI\Requests;

use Classes\PAPI\Responses\PurchaseCertificateResponse;
use Models\User;

class PurchaseCertificateRequest extends BaseRequest implements IPAPIRequest
{
    /**
     * @param User $user
     * @param array $params
     * @param bool $fromCache
     * @return array
     * @throws \Classes\Exceptions\PAPIException
     */
    public static function getResult(User $user, array $params = [], $fromCache = true): array
    {
        $result = static::execute('PurchaseCertificate', [
            'msisdn' => $user->getMsisdn()->value(),
            'price' => $params['price']
        ]);
        static::checkResultCode($result->HttpCode);
        return PurchaseCertificateResponse::parse($result->ResponseBody);
    }
}