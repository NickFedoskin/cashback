<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 17.11.2017
 * Time: 15:17
 */

namespace Classes\PAPI\Requests;


class WriteOffsRequest extends HistoryRequest
{
    protected static $historyKey = 'write-off';
}