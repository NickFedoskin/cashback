<?php
declare(strict_types = 1);

namespace Classes\PAPI\Requests;

use Classes\PAPI\Responses\GetCertificatesResponse;
use Models\User;

class GetCertificatesRequest extends BaseRequest implements IPAPIRequest
{
    /**
     * @param User $user
     * @param array $params
     * @param bool $fromCache
     * @return array
     * @throws \Classes\Exceptions\PAPIException
     */
    public static function getResult(User $user, array $params = [], $fromCache = true): array
    {
        $result = static::execute('GetCertificates', ['certificateType' => 'RTK']);
        static::checkResultCode($result->HttpCode);
        return GetCertificatesResponse::parse($result->ResponseBody);
    }
}