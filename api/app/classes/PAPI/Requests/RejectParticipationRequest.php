<?php
declare(strict_types = 1);

namespace Classes\PAPI\Requests;

use Models\User;

/**
 * Class RejectParticipationRequest
 * @package Classes\PAPI\Requests
 */
class RejectParticipationRequest extends BaseRequest implements IPAPIRequest
{
    static function getResult(User $user, array $params = [], $fromCache = true)
    {
        $msisdn = $user->getMsisdn()->value();
        $result = static::execute('RejectParticipation', ['msisdn' => $msisdn]);
        static::checkResultCode($result->HttpCode);
        return true;
    }
}