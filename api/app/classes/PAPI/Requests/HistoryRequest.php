<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 03.11.2017
 * Time: 14:01
 */

namespace Classes\PAPI\Requests;


use Classes\PAPI\Responses\HistoryResponse;
use Models\User;

class HistoryRequest extends BaseRequest implements IPAPIRequest
{

    protected static $historyKey = 'accrual';

    static function getResult(User $user, array $params = [], $fromCache = true)
    {
        $params['msisdn'] = $user->getMsisdn()->value();
        $result = static::execute('History', $params);
        static::checkResultCode($result->HttpCode);
        return HistoryResponse::parse($result->ResponseBody, static::$historyKey);
    }
}