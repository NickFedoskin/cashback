<?php
declare(strict_types = 1);

namespace Classes\PAPI\Requests;

use Models\User;
use Classes\PAPI\Responses\BalanceResponse;

/**
 * Запрос баланса, целей и статуса абонента.
 * Все приходит в одном запросе.
 *
 * Class BalanceRequest
 * @package Classes\PAPI\Requests
 */
class BalanceRequest extends BaseRequest implements IPAPIRequest
{
    public static function getResult(User $user, array $params = [], $fromCache = true): BalanceResponse
    {
        $response = static::execute('Balance', ['msisdn' => $user->getMsisdn()->value()]);
        static::checkResultCode($response->HttpCode);
        return BalanceResponse::parse($response->ResponseBody);
    }
}