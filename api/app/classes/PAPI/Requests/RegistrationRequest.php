<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 03.11.2017
 * Time: 13:48
 */

namespace Classes\PAPI\Requests;


use Models\User;

/*
 * Запрос на регистрацию абонента в услуге.
 */

class RegistrationRequest extends BaseRequest implements IPAPIRequest
{

    static function getResult(User $user, array $params = [], $fromCache = true)
    {
        $msisdn = $user->getMsisdn()->value();
        $result = static::execute('Registration', ['msisdn' => $msisdn]);
        static::checkResultCode($result->HttpCode);
        // Если ни каких ошибок до сих пор не случилось, значит все ок!
        return true;
    }
}