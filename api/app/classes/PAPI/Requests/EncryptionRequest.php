<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 27.11.2017
 * Time: 13:13
 */

namespace Classes\PAPI\Requests;


use Classes\Exceptions\AppException;
use Models\User;
use Phalcon\Cache\Backend\Redis;
use Phalcon\Di;

/*
 * Запрос шифрования msisdn.
 */

class EncryptionRequest extends BaseRequest implements IPAPIRequest
{

    static function getResult(User $user, array $params = [], $fromCache = true)
    {
        /** @var Redis $cache */
        $cache = Di::getDefault()->getShared('redis');
        $msisdn = $user->getMsisdn()->value();
        $cacheKey = $msisdn . '_msisdn_encrypt';
        if ($cache->exists($cacheKey) === false || $fromCache === false) {
            $result = static::execute('Encryption', ['msisdn' => $msisdn]);
            $encryptionResult = json_decode($result->ResponseBody, true);
            if (array_key_exists('encrypted_msisdn', $encryptionResult)) {
                $cache->save($cacheKey, $encryptionResult['encrypted_msisdn'], strtotime('1 hour') - time());
            } else {
                throw new AppException(500, 'Каталог невозможно сформировать.');
            }
        }
        return $cache->get($cacheKey);
    }
}