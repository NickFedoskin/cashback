<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 03.11.2017
 * Time: 13:02
 */

namespace Classes\PAPI\Requests;


use Models\User;

interface IPAPIRequest
{
    static function getResult(User $user, array $params = [], $fromCache = true);
}