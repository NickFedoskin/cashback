<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 03.11.2017
 * Time: 13:06
 */

namespace Classes\PAPI\Requests;

use Classes\PAPI\Responses\ValidationResponse;
use Models\User;

/*
 * Запрос не проверку возможности регистрации абонента
 */

class ValidationRequest extends BaseRequest implements IPAPIRequest
{
    public static function getResult(User $user, array $params = [], $fromCache = true)
    {
        $msisdn = $user->getMsisdn()->value();
        $result = static::execute('Validation', ['msisdn' => $msisdn]);
        static::checkResultCode($result->HttpCode);
        return ValidationResponse::parse($result->ResponseBody);
    }
}