<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 28.11.2017
 * Time: 19:44
 */

namespace Classes\PAPI;

/*
 * Ошибки, которые проксируются в приложение
 */
class PapiErrors
{
    private function __construct()
    {
    }

    const SERVICE_IS_UNAVAILABLE = [
        'Code' => 100,
        'Description' => 'Сервис временно недоступен, повторите позднее',
        'CodesIn' => []
    ];
    const TRY_AGAIN = [
        'Code' => 101,
        'Description' => 'Не удалось загрузить данные, попробуйте ещё раз',
        'CodesIn' => [400, 500]
    ];
    const REGISTRATION_IS_UNAVAILABLE = [
        'Code' => 102,
        'Description' => 'REGISTRATION_IS_UNAVAILABLE',
        'CodesIn' => [409]
    ];
    const USER_HAS_BLOCK = [
        'Code' => 103,
        'Description' => 'USER_HAS_BLOCK',
        'CodesIn' => [423]
    ];
    const ACCOUNT_NOT_FOUND = [
        'Code' => 104,
        'Description' => 'ACCOUNT_NOT_FOUND',
        'CodesIn' => [404]
    ];
    const NOT_REGISTERED = [
        'Code' => 105,
        'Description' => 'NOT_REGISTERED',
        'CodesIn' => []
    ];
    const CERTIFICATE_NOT_AVAILABLE = [
        'Code' => 118,
        'Description' => 'К сожалению, ваш запрос не выполнен. Попробуйте отправить запрос повторно или обратитесь в службу поддержки.',
        'CodesIn' => [404]
    ];
    const CERTIFICATE_BALANCE_ERROR = [
        'Code' => 122,
        'Description' => 'У вас недостаточно кэшбэка для заказа сертификата данного номинала. Попробуйте отправить запрос повторно.',
        'CodesIn' => [409]
    ];
    const CERTIFICATE_INDEF_ERROR = [
        'Code' => 123,
        'Description' => 'К сожалению, ваш запрос не был выполнен. Попробуйте повторить запрос повторно или обратитесь в службу поддержки.',
        'CodesIn' => [502]
    ];
}