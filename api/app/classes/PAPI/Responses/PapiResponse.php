<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 27.11.2017
 * Time: 10:45
 */

namespace Classes\PAPI\Responses;


class PapiResponse
{

    private function __construct()
    {
    }

    public static function parse(string $json)
    {
        $obj = json_decode($json, true);
        if ($obj != null) {
            $keys = array_keys($obj);
            $isValid = true;
            foreach (['HasInternalErrors', 'InternalErrors', 'Result'] as $key) {
                $isValid = $isValid && in_array($key, $keys);
            }
            if ($isValid) {
                $paResp = new PapiResponse();
                $paResp->HasInternalErrors = boolval($obj['HasInternalErrors']);
                $paResp->InternalErrors = $obj['InternalErrors'];
                $paResp->Result = PapiResponseResult::parse($obj['Result']);
                return $paResp;
            }
        }
        return null;
    }

    /** @var bool */
    public $HasInternalErrors;
    /** @var string[] */
    public $InternalErrors;
    /** @var PapiResponseResult */
    public $Result;
}

class PapiResponseResult
{
    private function __construct()
    {
    }

    public static function parse(array $papiResponseResult) : PapiResponseResult
    {
        if (isset($papiResponseResult['HttpCode']) && isset($papiResponseResult['ResponseBody'])) {
            $result = new PapiResponseResult();
            $result->HttpCode = $papiResponseResult['HttpCode'];
            $result->ResponseBody = $papiResponseResult['ResponseBody'];
            return $result;
        }
        return null;
    }

    /** @var int */
    public $HttpCode;
    /** @var string */
    public $ResponseBody;
}