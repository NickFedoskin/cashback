<?php
declare(strict_types = 1);

namespace Classes\PAPI\Responses;

class PurchaseCertificateResponse
{
    /**
     * @param string $json
     * @return array
     */
    public static function parse(string $json): array
    {
        $response = json_decode($json, true);
        $item = $response[0]['item'][0];
        return [
            'price' => intval($item['productOffering']['productOfferingPrice'][0]['price']['taxIncludedAmount']),
            'id' => $item['product']['productSerialNumber'],
            'expiryDate' => $item['product']['validFor']['endDateTime'],
        ];
    }

}

/* Пример json


[
   {
      "item": [
         {
            "state": "S_OK",
            "productOffering": {
               "category": [
                  {
                     "name": "CashbackCertificate"
                  }
               ],
               "productOfferingPrice": [
                  {
                     "price": {
                        "taxIncludedAmount": 1000
                     }
                  }
               ]
            },
            "product": {
               "productSerialNumber": "RTK-313044788003",
               "validFor": {
                  "endDateTime": "2018-04-04T00:00:00.000+0000"
               }
            }
         }
      ]
   }
]


*/