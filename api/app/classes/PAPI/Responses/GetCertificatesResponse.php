<?php
declare(strict_types = 1);

namespace Classes\PAPI\Responses;

class GetCertificatesResponse
{
    /**
     * @param string $json
     * @return array
     */
    public static function parse(string $json): array
    {
        $response = json_decode($json, true);
        $result = [];
        foreach ($response as $item) {
            $price = intval($item['productOfferingPrice'][0]['price']['taxIncludedAmount']);
            $result[] = $price;
        }
        sort($result);
        return $result;
    }

}

/* Пример json
[
    {
        "category": [
            {
                "name": "CashbackCertificate"
            }
        ],
        "productOfferingPrice": [
            {
                "price": {
                    "taxIncludedAmount": 100
                }
            }
        ]
    },
    {
        "category": [
            {
                "name": "CashbackCertificate"
            }
        ],
        "productOfferingPrice": [
            {
                "price": {
                    "taxIncludedAmount": 1000
                }
            }
        ]
    },
    {
        "category": [
            {
                "name": "CashbackCertificate"
            }
        ],
        "productOfferingPrice": [
            {
                "price": {
                    "taxIncludedAmount": 200
                }
            }
        ]
    }
]
*/