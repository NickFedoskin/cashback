<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 28.11.2017
 * Time: 18:26
 */

namespace Classes\PAPI\Responses;


use Classes\Exceptions\PAPIException;
use Classes\PAPI\PapiErrors;
use Classes\PAPI\UserStatus;
use Models\User;

class BalanceResponse
{
    public $UserStatus;
    /** @var int */
    public $BalanceValue;

    const RTK_PURPOSE_NAME = 'RTK';
    const CASHBACK_BALANCE_NAME = 'CashbackOperational';

    private function __construct()
    {
    }

    public static function parse(string $json)
    {

        $response = json_decode($json, true);
        if ($response != null) {
            if (isset($response[0])) {
                $loyaltyAccount = $response[0];
                if (isset($loyaltyAccount['accountStatus'])) {
                    $balance = new BalanceResponse();
                    $balance->UserStatus = UserStatus::parsePapiStatus($loyaltyAccount['accountStatus']);
                    if (isset($loyaltyAccount['loyaltyBalance'])) {
                        $mainBalance = -1;
                        $purposeBalance = -1;
                        foreach ($loyaltyAccount['loyaltyBalance'] as $item) {
                            if (isset($item['name']) && isset($item['balance'])) {
                                if (isset($item['balance']['amount'])) {
                                    $bValue = $item['balance']['amount'];
                                    switch ($item['name']) {
                                        case static::CASHBACK_BALANCE_NAME:
                                            $mainBalance = $bValue;
                                            break;
                                        case static::RTK_PURPOSE_NAME:
                                            $purposeBalance = $bValue;
                                            break;
                                    }
                                }
                            }
                        }
                        if ($mainBalance != -1) {
                            $balance->BalanceValue = $mainBalance;
                            $isIndividual = true;
                            if ($purposeBalance != -1) {
                                $isIndividual = false;
                                $balance->BalanceValue += $purposeBalance;
                            }
                            $user = User::current();
                            $hasRegistration = $balance->UserStatus == UserStatus::REGISTERED
                                || $balance->UserStatus == UserStatus::IN_PROGRESS;
                            if ($hasRegistration === true && $user->IsIndividual() !== $isIndividual) {
                                $user->setIsIndividual($isIndividual);
                                $user->save();
                            }
                            return $balance;
                        }
                    }
                }
            }
        }
        return null;
    }

    public function registrationCheck()
    {
        $hasRegistration = $this->UserStatus == UserStatus::REGISTERED
            || $this->UserStatus == UserStatus::IN_PROGRESS;
        if ($hasRegistration === false) {
            throw new PAPIException(PapiErrors::NOT_REGISTERED);
        }
    }

}

/* Пример json
[
  {
    "accountStatus": "MemberFreeCom",
    "name": "Cashback",
    "loyaltyBalance": [
      {
        "name": "CashbackOperational",
        "balance": {
          "amount": 1700
        }
      },
      {
        "name": "CashbackEarnTotal",
        "balance": {
          "amount": 12698
        }
      },
      {
        "name": "CashbackBurnTotal3",
        "balance": {
          "amount": 10998
        }
      },
      {
        "name": "RTK",
        "description": "RTK",
        "balanceUsageRule": [
          {
            "description": "Purpose",
            "characteristic": [
              {
                "name": "Priority",
                "value": "1"
              },
              {
                "name": "PurposeLimit"
              }
            ]
          }
        ],
        "balance": {
          "amount": 12698
        }
      }
    ]
  }
]
 */