<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 28.11.2017
 * Time: 18:18
 */

namespace Classes\PAPI\Responses;


use Classes\PAPI\UserStatus;
use Models\User;

class ValidationResponse
{
    public $Party;
    public $UserStatus;

    private function __construct()
    {
    }

    public static function parse(string $json)
    {
        $response = json_decode($json, true);
        if (isset($response[0]) && isset($response[0]["party"])) {
            $validation = new ValidationResponse();
            $validation->Party = $response[0]["party"];
            if (isset($response[0]['loyaltyAccount']) && isset($response[0]['loyaltyAccount'][0]) && isset($response[0]['loyaltyAccount'][0]['accountStatus'])) {
                $validation->UserStatus = UserStatus::parsePapiStatus($response[0]['loyaltyAccount'][0]['accountStatus']);
                return $validation;
            }
        }
        return null;
    }

}

/* Пример json
[
  {
    "loyaltyAccount": [
        {
            "accountType": "LoyaltyAccount",
            "accountStatus": "NotMemberFreeCom",
            "name": "Cashback"
        }
    ],
    "party": {
        "type": "Individual"
    }
  }
]
*/