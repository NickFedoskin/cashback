<?php
declare(strict_types=1);

namespace Classes\PAPI\Responses;

use Classes\Helper;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Di;

define('WRITE_OFF', 'write-off');
define('ACCRUAL', 'accrual');

class HistoryResponse
{
    /** @var  array */
    protected $parsedHistory;
    /** @var  Mysql */
    protected $dbh;
    /** @var array */
    protected $parsedMonths = [
        WRITE_OFF => [],
        ACCRUAL => []
    ];

    /**
     * HistoryResponse constructor
     */
    private function __construct() {}

    /**
     * @return mixed
     */
    public function toArray(): array
    {
        return $this->parsedHistory;
    }

    /**
     * @return Mysql
     */
    protected function db(): Mysql
    {
        if ($this->dbh == null) {
            $this->dbh = Di::getDefault()->getShared('db');
        }
        return $this->dbh;
    }

    /**
     * @param string $json
     * @param string $historyType
     * @return HistoryResponse
     */
    //ToDo: Refactor this after
    public static function parse(string $json, string $historyType): HistoryResponse
    {
        $historyInst = new HistoryResponse();
        $response = [
            ACCRUAL => [],
            WRITE_OFF => [],
        ];
        $parsedHistory = $historyInst->parseHistory($json);
        foreach ([ACCRUAL, WRITE_OFF] as $key) {
            if (isset($parsedHistory[$key]) && count($parsedHistory[$key]) > 0) {
                $result = $parsedHistory[$key];
                $newArr = [];
                $months = array_keys($historyInst->parsedMonths[$key]);
                $currentDate = strtotime(date('Y-m'));
                foreach ($result as $item) {
                    $item['Balance'] = Helper::formatMoney($item['Balance']);
                    usort($item['Categories'], function ($cat1, $cat2) {
                        return $cat1['Order'] > $cat2['Order'];
                    });
                    foreach ($item['Categories'] as &$category) {
                        arsort($category['Accruals']);
                        $category['Accruals'] = array_keys($category['Accruals']);
                        $category['Amount'] = Helper::formatMoney($category['Amount']);
                    }
                    usort($item['Days'], function ($day1, $day2) {
                        return $day1['Order'] < $day2['Order'];
                    });
                    foreach ($item['Days'] as &$day) {
                        arsort($day['Accruals']);
                        $day['Accruals'] = array_keys($day['Accruals']);
                    }
                    $monthTitle = $item['MonthTitle'];
                    $item['MonthTitle'] = $historyInst->getDateTitle($item['MonthTitle']);
                    $newArr[] = $item;
                    while (true) {
                        $nextMonth = date('Y-m', strtotime($monthTitle . '+ 1 month'));
                        if (!in_array($nextMonth, $months) && $currentDate >= strtotime($nextMonth)) {
                            $newArr[] = $historyInst->getEmptyMonth($nextMonth, true);
                            $monthTitle = $nextMonth;
                        } else {
                            break;
                        }
                    }
                }
                $result = $newArr;
            } else if ($parsedHistory != 'error') {
                $result = [$historyInst->getEmptyMonth('', true)];
            } else {
                $result = $parsedHistory;
            }
            $response[$key] = $result;
        }

        $historyInst->parsedHistory = $response[$historyType];
        return $historyInst;
    }

    /**
     * @param string $date
     * @param bool $balanceFormatted
     * @return array
     */
    function getEmptyMonth(string $date = '', $balanceFormatted = false): array
    {
        if (empty($date)) {
            $date = date('Y-m');
        }
        return [
            'MonthTitle' => $this->getDateTitle($date),
            'Balance' => $balanceFormatted ? Helper::formatMoney('0') : 0,
            'Accruals' => [],
            'Categories' => [],
            'Days' => []
        ];
    }

    /**
     * @param string $history
     * @return array
     */
    //ToDo: Refactor this after
    protected function parseHistory(string $history): array
    {
        $result = [];
        $history = json_decode($history, true);
        if (isset($history[0]) && isset($history[0]['loyaltyTransaction'])) {
            foreach ($history[0]['loyaltyTransaction'] as $loyaltyTransaction) {
                if (isset($loyaltyTransaction['type']) && isset($loyaltyTransaction['characteristic'])
                    && isset($loyaltyTransaction['transactionDateTime']) && isset($loyaltyTransaction['quantity'])
                ) {
                    $historyType = $loyaltyTransaction['type'] == 'LoyaltyEarn' ? ACCRUAL : WRITE_OFF;
                    $fields = [
                        'PartnerName' => '',
                        'ProductCatalogSectionId' => 0,
                        'SourceAccount.AccountCode' => '',
                        'EventDate' => '',
                        'BalanceTypeCode' => '',
                        'GiftCode' => '',
                        'GiftPrice' => '',
                        'DateTo' => ''
                    ];
                    foreach ($loyaltyTransaction['characteristic'] as $characteristic) {
                        if (isset($characteristic['name']) && isset($characteristic['value'])) {
                            $name = $characteristic['name'];
                            $value = $characteristic['value'];
                            if (in_array($name, array_keys($fields))) {
                                $fields[$name] = $value;
                            }
                        }
                    }

                    $date = $fields['EventDate'];
                    $date = explode(':', $date);
                    $date = "{$date[0]}:{$date[1]}";
                    $dateTitle = $this->dateFormat($date, 'Y-m');
                    $index = array_search($dateTitle, array_keys($this->parsedMonths[$historyType]));
                    if ($index === false) {
                        $index = count($this->parsedMonths[$historyType]);
                        $this->parsedMonths[$historyType][$dateTitle] = [
                            'Days' => [],
                            'Categories' => []
                        ];
                        $result[$historyType][$index] = [
                            'MonthTitle' => $dateTitle,
                            'Balance' => 0,
                            'Accruals' => [],
                            'Categories' => [],
                            'Days' => []
                        ];
                    }
                    $newBalance = 0;
                    if (isset($loyaltyTransaction['quantity']['amount'])) {
                        $newBalance = intval($loyaltyTransaction['quantity']['amount']);
                    }

                    if ($historyType == WRITE_OFF) {
                        $newBalance *= -1;
                        $fields['PartnerName'] = 'Пополнение баланса вашего номера';
                        $fields['ProductCatalogSectionId'] = 100;
                        if ($fields['BalanceTypeCode'] == 'RTK')
                        {
                            $fields['PartnerName'] = 'Сертификат на смартфон';
                            $fields['ProductCatalogSectionId'] = 101;
                        }
                    }

                    $result[$historyType][$index]['Balance'] += $newBalance;
                    $item = [
                        'EventDate' => $this->dateFormat($date, 'H:i'),
                        'CategoryDate' => $this->getDayTitle($date) . ' ' . $this->dateFormat($date, 'H:i'),
                        'PartnerName' => $fields['PartnerName'],
                        'Amount' => Helper::formatMoney($newBalance),
                        'type' => $fields['BalanceTypeCode'],
                    ];
                    if ($fields['BalanceTypeCode'] == 'RTK')
                    {
                        $item['price'] = intval($fields['GiftPrice']);
                        $item['id'] = $fields['GiftCode'];
                        $item['expiryDate'] = $fields['DateTo'];
                        if ($item['id'] == "0") continue;
                    }
                    $result[$historyType][$index]['Accruals'][] = $item;

                    $accrualIndex = count($result[$historyType][$index]['Accruals']) - 1;

                    $dayTitle = $this->getDayTitle($date);
                    $dayIndex = array_search($dayTitle, $this->parsedMonths[$historyType][$dateTitle]['Days']);
                    if ($dayIndex === false) {
                        $dayIndex = count($this->parsedMonths[$historyType][$dateTitle]['Days']);
                        $this->parsedMonths[$historyType][$dateTitle]['Days'][] = $dayTitle;
                        $result[$historyType][$index]['Days'][$dayIndex] = [
                            'DayTitle' => $dayTitle,
                            'Accruals' => [],
                            'Order' => strtotime($date)
                        ];
                    }
                    $result[$historyType][$index]['Days'][$dayIndex]['Accruals'][$accrualIndex] = strtotime($date);
                    $catId = $fields['ProductCatalogSectionId'];

                    if ($catId != 0) {
                        $catIndex = array_search($catId, $this->parsedMonths[$historyType][$dateTitle]['Categories']);
                        if ($catIndex === false) {
                            $category = $this->getCategory($catId);
                            $catIndex = count($this->parsedMonths[$historyType][$dateTitle]['Categories']);
                            $this->parsedMonths[$historyType][$dateTitle]['Categories'][] = $catId;
                            $result[$historyType][$index]['Categories'][$catIndex] = [
                                'Category' => $category['title'],
                                'Icon' => $category['icon'],
                                'Accruals' => [],
                                'Amount' => 0,
                                'Order' => $category['priority']
                            ];
                        }
                        $result[$historyType][$index]['Categories'][$catIndex]['Amount'] += $newBalance;
                        $result[$historyType][$index]['Categories'][$catIndex]['Accruals'][$accrualIndex] = strtotime($date);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param string $date
     * @return string
     */
    protected function getDateTitle(string $date): string
    {
        return $this->monthName(intval($this->dateFormat($date, 'n'))) . $this->dateFormat($date, ' Y');
    }

    /**
     * @param string $date
     * @return string
     */
    protected function getDayTitle(string $date): string
    {
        return $this->dateFormat($date, 'j ') . $this->monthPlural(intval($this->dateFormat($date, 'n')));
    }

    /**
     * @param string $date
     * @param string $format
     * @return string
     */
    protected function dateFormat(string $date, string $format): string
    {
        $timestamp = strtotime($date);
        if ($timestamp == false) {
            $timestamp = 0;
        }
        return date($format, $timestamp);
    }

    /**
     * @param int $monthNum
     * @return string
     */
    protected function monthName(int $monthNum): string
    {
        $month = array(
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь'
        );
        return $month[$monthNum];
    }

    /**
     * @param int $monthNum
     * @return string
     */
    protected function monthPlural(int $monthNum): string
    {
        $monthPlural = array(
            1 => 'Января',
            2 => 'Февраля',
            3 => 'Марта',
            4 => 'Апреля',
            5 => 'Мая',
            6 => 'Июня',
            7 => 'Июля',
            8 => 'Августа',
            9 => 'Сентября',
            10 => 'Октября',
            11 => 'Ноября',
            12 => 'Декабря'
        );
        return $monthPlural[$monthNum];
    }

    /**
     * @param int $catId
     * @return mixed
     */
    protected function getCategory(int $catId)
    {
        $baseUrl = Di::getDefault()->getShared('baseUrl');
        $stmt = $this->db()->prepare("SELECT Name as title, concat(\"$baseUrl\", Icon) as icon, Priority as priority FROM Section WHERE XML_ID=:id and ParentSectionID = 0");
        $stmt->execute(['id' => $catId]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

}

/*
[
  {
    "loyaltyAccount": {
      "accountType": "LoyaltyAccount",
      "loyaltyProgramMember": {
        "type": "LoyaltyProgramMember",
        "characteristic": [
          {
            "name": "AccountIdentifierCode",
            "value": "79154304534"
          },
          {
            "name": "AccountIdentifierTypeCode",
            "value": "MSISDN"
          }
        ]
      }
    },
    "loyaltyTransaction": [
      {
        "type": "LoyaltyEarn",
        "description": "Начисление",
        "id": "784",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "P"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "CashBack"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "RequestReason",
            "value": "Начисления средств на баланс"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T14:01:22+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T11:01:22+03:00"
          },
          {
            "name": "PartnerName",
            "value": "МТС"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "6"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "0"
          },
          {
            "name": "CashbackAmount",
            "value": "10"
          },
          {
            "name": "CalculationMethodType",
            "value": "FixedValue"
          },
          {
            "name": "Value",
            "value": "10.0"
          },
          {
            "name": "Percent",
            "value": "0.0"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "1970-01-01T00:00:01+03:00"
          }
        ],
        "closingBalance": 10,
        "transactionDateTime": "2017-12-04T11:01:22.000+0000",
        "quantity": {
          "amount": 10
        }
      },
      {
        "type": "LoyaltyEarn",
        "description": "Начисление",
        "id": "791",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "P"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "CashBack"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "RequestReason",
            "value": "Начисления средств на баланс"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T15:21:39+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T13:50:59+03:00"
          },
          {
            "name": "PartnerName",
            "value": "THEBODYSHOP.RU"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "1"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "1"
          },
          {
            "name": "CashbackAmount",
            "value": "10000"
          },
          {
            "name": "CalculationMethodType",
            "value": "Percent"
          },
          {
            "name": "Value",
            "value": "0.0"
          },
          {
            "name": "Percent",
            "value": "10.0"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "1970-01-01T00:00:01+03:00"
          }
        ],
        "closingBalance": 1000,
        "transactionDateTime": "2017-12-04T12:21:39.000+0000",
        "quantity": {
          "amount": 1000
        }
      },
      {
        "type": "LoyaltyEarn",
        "description": "Начисление",
        "id": "792",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "P"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "CashBack"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "RequestReason",
            "value": "Начисления средств на баланс"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T15:21:40+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T13:40:59+03:00"
          },
          {
            "name": "PartnerName",
            "value": "OneTwoTrip"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "2"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "0"
          },
          {
            "name": "CashbackAmount",
            "value": "12300"
          },
          {
            "name": "CalculationMethodType",
            "value": "Percent"
          },
          {
            "name": "Value",
            "value": "0.0"
          },
          {
            "name": "Percent",
            "value": "3.0"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "1970-01-01T00:00:01+03:00"
          }
        ],
        "closingBalance": 369,
        "transactionDateTime": "2017-12-04T12:21:40.000+0000",
        "quantity": {
          "amount": 369
        }
      },
      {
        "type": "LoyaltyEarn",
        "description": "Начисление",
        "id": "793",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "P"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "CashBack"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "RequestReason",
            "value": "Начисления средств на баланс"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T15:21:40+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T13:45:59+03:00"
          },
          {
            "name": "PartnerName",
            "value": "Нияма"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "3"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "0"
          },
          {
            "name": "CashbackAmount",
            "value": "11410"
          },
          {
            "name": "CalculationMethodType",
            "value": "Percent"
          },
          {
            "name": "Value",
            "value": "0.0"
          },
          {
            "name": "Percent",
            "value": "2.0"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "1970-01-01T00:00:01+03:00"
          }
        ],
        "closingBalance": 228,
        "transactionDateTime": "2017-12-04T12:21:40.000+0000",
        "quantity": {
          "amount": 228
        }
      },
      {
        "type": "LoyaltyBurn",
        "description": "Списание",
        "id": "794",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "R"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "BalanceName"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "CampaignName",
            "value": "CashBack"
          },
          {
            "name": "RequestReason",
            "value": "Списание на цель"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "PartnerName",
            "value": "PartnerName"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "0"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "0"
          },
          {
            "name": "CashbackAmount",
            "value": "0"
          },
          {
            "name": "Value",
            "value": "0.0"
          },
          {
            "name": "Percent",
            "value": "0.0"
          },
          {
            "name": "ServiceCode",
            "value": "FREECOM"
          },
          {
            "name": "ServiceName",
            "value": "Бесплатная связь"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "2017-12-04T15:24:10+03:00"
          }
        ],
        "closingBalance": 0,
        "transactionDateTime": "2017-12-04T12:24:10.000+0000",
        "quantity": {
          "amount": 10
        }
      },
      {
        "type": "LoyaltyBurn",
        "description": "Списание",
        "id": "795",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "R"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "BalanceName"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "CampaignName",
            "value": "CashBack"
          },
          {
            "name": "RequestReason",
            "value": "Списание на цель"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "PartnerName",
            "value": "PartnerName"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "0"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "0"
          },
          {
            "name": "CashbackAmount",
            "value": "0"
          },
          {
            "name": "Value",
            "value": "0.0"
          },
          {
            "name": "Percent",
            "value": "0.0"
          },
          {
            "name": "ServiceCode",
            "value": "FREECOM"
          },
          {
            "name": "ServiceName",
            "value": "Бесплатная связь"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "2017-12-04T15:24:10+03:00"
          }
        ],
        "closingBalance": 0,
        "transactionDateTime": "2017-12-04T12:24:10.000+0000",
        "quantity": {
          "amount": 1000
        }
      },
      {
        "type": "LoyaltyBurn",
        "description": "Списание",
        "id": "796",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "R"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "BalanceName"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "CampaignName",
            "value": "CashBack"
          },
          {
            "name": "RequestReason",
            "value": "Списание на цель"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "PartnerName",
            "value": "PartnerName"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "0"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "0"
          },
          {
            "name": "CashbackAmount",
            "value": "0"
          },
          {
            "name": "Value",
            "value": "0.0"
          },
          {
            "name": "Percent",
            "value": "0.0"
          },
          {
            "name": "ServiceCode",
            "value": "FREECOM"
          },
          {
            "name": "ServiceName",
            "value": "Бесплатная связь"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "2017-12-04T15:24:10+03:00"
          }
        ],
        "closingBalance": 0,
        "transactionDateTime": "2017-12-04T12:24:10.000+0000",
        "quantity": {
          "amount": 369
        }
      },
      {
        "type": "LoyaltyBurn",
        "description": "Списание",
        "id": "797",
        "characteristic": [
          {
            "name": "BalanceTypeCode",
            "value": "CB"
          },
          {
            "name": "BalanceChangeTypeCode",
            "value": "R"
          },
          {
            "name": "BalanceChangeTypeName",
            "value": "BalanceName"
          },
          {
            "name": "ChannelCode",
            "value": "SMBP"
          },
          {
            "name": "ChannelName",
            "value": "SMBP"
          },
          {
            "name": "CampaignName",
            "value": "CashBack"
          },
          {
            "name": "RequestReason",
            "value": "Списание на цель"
          },
          {
            "name": "SourceAccount.AccountTypeCode",
            "value": "MSISDN"
          },
          {
            "name": "SourceAccount.AccountCode",
            "value": "79154304534"
          },
          {
            "name": "RequestDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "EventDate",
            "value": "2017-12-04T15:24:10+03:00"
          },
          {
            "name": "PartnerName",
            "value": "PartnerName"
          },
          {
            "name": "ProductCatalogSectionId",
            "value": "0"
          },
          {
            "name": "ProductCatalogSubSectionId",
            "value": "0"
          },
          {
            "name": "CashbackAmount",
            "value": "0"
          },
          {
            "name": "Value",
            "value": "0.0"
          },
          {
            "name": "Percent",
            "value": "0.0"
          },
          {
            "name": "ServiceCode",
            "value": "FREECOM"
          },
          {
            "name": "ServiceName",
            "value": "Бесплатная связь"
          },
          {
            "name": "ServiceProvisionId",
            "value": "0"
          },
          {
            "name": "GiftPrice",
            "value": "0"
          },
          {
            "name": "DateFrom",
            "value": "2017-12-04T15:24:10+03:00"
          }
        ],
        "closingBalance": 0,
        "transactionDateTime": "2017-12-04T12:24:10.000+0000",
        "quantity": {
          "amount": 228
        }
      }
    ]
  }
]
 */