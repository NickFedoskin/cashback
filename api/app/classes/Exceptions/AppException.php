<?php
namespace Classes\Exceptions;


use Phalcon\Exception;

class AppException extends Exception
{
    /** @var  array Error content */
    private $_content;
    private $_title;

    public function __construct(int $code, string $message, string $title = '', array $content = [], \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_content = $content;
        $this->_title = $title;
    }

    /**
     * Returns content array.
     * @return array
     */
    public function getContent() : array
    {
        return $this->_content;
    }

    public function getTitle()
    {
        if (!isset($this->_title) || empty($this->_title)) {
            $path = explode('\\', static::class);
            $this->_title = array_pop($path);
        }
        return $this->_title;
    }
}