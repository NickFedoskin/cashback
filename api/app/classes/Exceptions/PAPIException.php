<?php


namespace Classes\Exceptions;


class PAPIException extends AppException
{
    public function __construct(array $papiError)
    {
        parent::__construct(500, $papiError['Description'], '', ['Code' => $papiError['Code']]);
    }

}