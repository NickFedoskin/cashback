<?php


namespace Classes\Exceptions;


class MissingParameterException extends AppException
{
    public function __construct($param)
    {
        parent::__construct(
            400,
            "Missing required parameter - $param");
    }

}