<?php
declare(strict_types = 1);

namespace Classes;

class Helper
{
    /**
     * @param $sum
     * @return string
     */
    public static function formatMoney($sum): string
    {
        return number_format($sum, 0, '.', ' ') . ' ₽';
    }

    /**
     * @return int
     */
    public static function defaultCacheLifeTime(): int
    {
        return strtotime('1 hour') - time();
    }

    /**
     * Returns true, if $v1 greater or equal to $v2.
     * @param string $v1
     * @param string $v2
     * @return bool
     */
    public static function compareVersion(string $v1, string $v2): bool
    {
        $v1 = intval(str_replace('.', '', $v1));
        $v2 = intval(str_replace('.', '', $v2));
        return $v1 >= $v2;
    }

    /**
     * @param \DateTime $startDate
     * @return string
     */
    public static function nextQuarterEndDate($startDate = null): string
    {
        $now = $startDate !== null ? $startDate : new \DateTime(date('c'));
        $month = intval($now->format('n'));
        $year = intval($now->format('Y'));
        if ($month <= 3) {
            $nextQuarterEndMonth = 6;
        } elseif ($month <= 6) {
            $nextQuarterEndMonth = 9;
        } elseif ($month <= 9) {
            $nextQuarterEndMonth = 12;
        } else {
            $nextQuarterEndMonth = 3;
            $year += 1;
        }
        $resultDateStr = sprintf('01-%s-%s', $nextQuarterEndMonth, $year);
        $result = new \DateTime(date('d-n-Y', strtotime($resultDateStr)));
        $result = $result->modify('last day of');
        return $result->format('c');
    }

    /**
     * @param string $host
     * @param int $port
     * @param int $timeout
     * @return int|mixed
     */
    public static function pingHost(string $host, int $port = 80, int $timeout = 3): int
    {
        $starTime = microtime(true);
        $socket = fsockopen($host, $port, $errno, $errstr, $timeout);
        $stopTime = microtime(true);
        if (!$socket) {
            return -1;
        }

        fclose($socket);
        return ($stopTime - $starTime) * 1000;
    }
}