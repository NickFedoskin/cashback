<?php
/**
 * Created by PhpStorm.
 * User: vaulin.a
 * Date: 22.11.2017
 * Time: 12:21
 */

namespace Classes;


use Classes\Exceptions\AppException;
use Models\User;

class Mail
{
    const SMTP_HOST = 'mail.inside.mts.ru';
    const SMTP_PORT = '25';
    const SMTP_CHARSET = 'utf-8';
    const SUPPORT_EMAIL = 'myfeedback@mts.ru';

    public static function send($usrEmail, $usrSubj, $usrMsg)
    {
        $user = User::current();
        $subject = 'Бесплатная связь_мобильное приложение';
        $SEND = "Date: " . date("D, d M Y H:i:s") . " UT\r\n";
        $SEND .= 'Content-Type: text/plain; charset=UTF-8' . "\r\n";
        $SEND .= 'Subject: =?' . static::SMTP_CHARSET . '?B?' . base64_encode($subject) . "=?=\r\n";
        $SEND .= "To: <" . static::SUPPORT_EMAIL . ">\r\n";
        $SEND .= "From: <" . $usrEmail . ">\r\n";
        $SEND .= "Тема: $usrSubj\r\nНомер телефона: {$user->getMsisdn()->value()}\r\nEmail: $usrEmail\r\nТекст вопроса:\r\n$usrMsg\r\n";

        // Отправка почты через сокеты
        if (!$socket = fsockopen(static::SMTP_HOST, static::SMTP_PORT, $errno, $errstr, 30)) {
            $error = $errno . " " . $errstr;
        } else {
            if (static::server_parse($socket, "220") === false) {
                $error = 'while connecting to SMTP server';
            } else {
                $msgs = [
                    [
                        'text' => "HELO " . static::SMTP_HOST . "\r\n",
                        'response' => '250'
                    ],
                    [
                        'text' => "MAIL FROM: <" . $usrEmail . ">\r\n",
                        'response' => '250'
                    ],
                    [
                        'text' => "RCPT TO: <" . static::SUPPORT_EMAIL . ">, <Yuliya.Silina@mts.ru>, <mnrogozh@mts.ru>, <yyshapki@mts.ru>, <sskamal1@mts.ru>\r\n",
                        'response' => '250'
                    ],
                    [
                        'text' => "DATA\r\n",
                        'response' => '354'
                    ],
                    [
                        'text' => $SEND . "\r\n.\r\n",
                        'response' => '250'
                    ]
                ];

                foreach ($msgs as $msg) {
                    fputs($socket, $msg['text']);
                    if (static::server_parse($socket, $msg['response']) === false) {
                        $error = 'while sending ' . $msg['text'];
                        fclose($socket);
                        break;
                    }
                }
                if (empty($error)) {
                    fputs($socket, "QUIT\r\n");
                    fclose($socket);
                    return ['Message has been sent'];
                }
            }
        }
        $error = [
            'msisdn' => $user->getMsisdn()->value(),
            'message' => 'Error occurs ' . $error
        ];
        Syslog::write('mail_send_error', json_encode($error));
        throw new AppException(500, 'Error on mail send');
    }

    // Тут происходят какие-то шаманства
    protected static function server_parse($socket, $response)
    {
        while (@substr($server_response, 3, 1) != ' ') {
            if (!($server_response = fgets($socket, 256))) {
                return false;
            }
        }
        if (substr($server_response, 0, 3) != $response) {
            return false;
        }
        return true;
    }

}

