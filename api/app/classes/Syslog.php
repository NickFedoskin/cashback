<?php
namespace Classes;

use Phalcon\Exception;

class Syslog
{
    static $hosts = ["127.0.0.1"];
    static $port = 514;
    static $timeOut = 1;

    const EMERGENCY = 0; //system is unusable
    const ALERT = 1; //action must be taken immediately
    const CRITICAL = 2; //critical conditions
    const ERROR = 3; //error conditions
    const WARNING = 4; //warning conditions
    const NOTICE = 5; //normal but significant condition
    const INFORMATIONAL = 6; //informational messages
    const DEBYG = 7; //debug-level messages


    const LOCAL = 16; //local Facility use 0

    const LIMIT = 7000;


    static function write($tag, $message, $severity = 6)
    {
        $mk_time = microtime();
        $actualtime = time();
        $month = date("M", $actualtime);
        $day = substr("  " . date("j", $actualtime), -2);
        $hhmmss = date("H:i:s", $actualtime);
        $timestamp = $month . " " . $day . " " . $hhmmss;

        foreach (self::$hosts as $host) {
            $header = "{$timestamp} {$host}";
            try {
                $fp = fsockopen("udp://{$host}", self::$port, $errno, $errstr, self::$timeOut);
                if ($fp) {
                    stream_set_timeout($fp, 0, 300000);//0.3 сек
                    $num = self::LOCAL * 8 + $severity;
                    $to_send = "";

                    $len = strlen($message);

                    if ($len > self::LIMIT) {
                        $to_send = "{$mk_time} ";
                        for ($i = 0; $i < $len; $i++) {
                            $to_send .= $message{$i};
                            if (strlen($to_send) >= self::LIMIT) {
                                $msg = "<{$num}>{$header} {$tag} {$to_send}";
                                $to_send = "{$mk_time} ";
                                fwrite($fp, $msg);
                            }
                        }
                    } else
                        $to_send = $message;

                    $msg = "<{$num}>{$header} {$tag} {$to_send}";
                    if ($to_send)
                        fwrite($fp, $msg);
                    fclose($fp);
                }
            } catch (Exception $e) {
                // print_r($e);
            }
        }

    }
}

?>
