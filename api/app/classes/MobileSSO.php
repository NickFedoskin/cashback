<?php


namespace Classes;

use Classes\Exceptions\AppException;
use Models\Token;
use Models\User;
use Phalcon\Di;

define('DOMAIN_PLACE_HOLDER', '{domain}');
define('REDIRECT_URL_TEMPLATE', 'https://' . DOMAIN_PLACE_HOLDER . '/sso/oauth2/callback');

class MobileSSO
{

    public static function getStartAuthLink() : string
    {
        $config = Di::getDefault()->getConfig();
        $redirect = urlencode(str_replace(DOMAIN_PLACE_HOLDER, $config->APP->domain . '/apinew', REDIRECT_URL_TEMPLATE));
        return "https://login.mts.ru/amserver/oauth2/auth?client_id=" . $config->mobile_sso->clientId . "&scope=openid%20profile%20mobile&redirect_uri=$redirect&response_type=code&display=touch&state=1";
    }

    public static function finishAuth(array $queryParams) : Token
    {
        if (!array_key_exists('error', $queryParams) && array_key_exists('code', $queryParams)) {
            $config = Di::getDefault()->getConfig();
            #region Request token
            $serverResponse = static::curl($config->mobile_sso->url . '/token', [
                'grant_type' => 'authorization_code',
                'redirect_uri' => str_replace(DOMAIN_PLACE_HOLDER, $config->APP->domain . '/apinew', REDIRECT_URL_TEMPLATE),
                'client_id' => $config->mobile_sso->clientId,
                'client_secret' => $config->mobile_sso->clientSecret,
                'code' => $queryParams['code']
            ]);
            #endregion

            $serverResponse = json_decode($serverResponse, true);
            if ($serverResponse != null && array_key_exists('access_token', $serverResponse)) {
                $token = $serverResponse['access_token'];
                #region Request msisdn
                $serverResponse = static::curl($config->mobile_sso->url . '/api', [], ["Authorization: Bearer $token"]);
                $serverResponse = json_decode($serverResponse, true);
                #endregion
                if ($serverResponse != null && array_key_exists('mobile:phone', $serverResponse)) {
                    $msisdn = new MSISDN('7' . $serverResponse['mobile:phone']);
                    $user = User::getUserByMSISDN($msisdn);
                    $user->setIsIndividual(boolval($serverResponse['profile:name:type'] == 'Person'));
                    $user->setTerminalId(intval($serverResponse['mobile:terminal:id']));
                    $user->save();
                    return Token::fromString($user, $token);
                }
            }
        }
        throw new AppException(401, 'Authorization failed');
    }


    public static function curl($url, $params = [], $headers = [], $urldecode = true, $delete = false)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        if ($params) {
            curl_setopt($curl, CURLOPT_POST, 1);
            $params = $urldecode ? urldecode(http_build_query($params)) : json_encode($params);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

            curl_setopt($curl, CURLOPT_FILE, fopen('php://stdout', 'w'));
        }
        $headers[] = 'Host: login.mts.ru';
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        $requestDump = [
            'url' => $url,
            'headers' => $headers,
            'time' => curl_getinfo($curl, CURLINFO_TOTAL_TIME),
            'body' => $params
        ];
        if (curl_errno($curl)) {
            Syslog::write('mobile-sso-error', json_encode([
                'request' => $requestDump,
                'error' => [
                    'code' => curl_errno($curl),
                    'message' => curl_error($curl)
                ]
            ]));
        } else {
            Syslog::write('mobile-sso', json_encode([
                'request' => $requestDump,
                'response' => [
                    'httpCode' => curl_getinfo($curl, CURLINFO_HTTP_CODE),
                    'body' => $result
                ]
            ]));
        }
        curl_close($curl);

        return $result;
    }

}