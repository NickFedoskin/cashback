<?php
namespace Classes;


use Classes\Exceptions\NotFoundException;
use Classes\PAPI\Requests\EncryptionRequest;
use Models\User;
use Phalcon\Http\Request;

class MSISDN
{
    /** @var  string */
    private $_msisdn;

    public function __construct(string $msisdn)
    {
        /* TODO: check format*/
        $this->_msisdn = $msisdn;
    }

    public static function fromHeaders() : MSISDN
    {
        $request = new Request();
        $msisdn = $request->getHeader('X-Msisdn');
        return static::fromString($msisdn);
    }

    /** @var MSISDN for current request won't change. */
    private static $_current;

    public static function fromRequest() : MSISDN
    {
        if (static::$_current == null) {
            RequestParams::containsKeys(['Msisdn']);
            $msisdn = RequestParams::get('Msisdn');
            static::$_current = static::fromString($msisdn);
        }
        return static::$_current;
    }

    public static function fromString(string $msisdn)
    {
        if (empty($msisdn)) {
            throw new NotFoundException('MSISDN not found');
        }
        return new MSISDN($msisdn);
    }

    public function value()
    {
        return $this->_msisdn;
    }

    public function encrypt()
    {
        return EncryptionRequest::getResult(User::current());
    }

}