<?php
namespace Classes;

use Classes\Exceptions\MissingParameterException;

class RequestParams
{
    private function __construct()
    {
    }

    /** @var  array */
    private static $_params;

    public static function set(array $params)
    {
        static::$_params = $params;
    }

    public static function has(string $key)
    {
        return array_key_exists($key, static::$_params);
    }

    public static function get(string $key)
    {
        return static::$_params[$key];
    }

    public static function containsKeys(array $keys) : bool
    {
        foreach ($keys as $key) {
            if (!static::has($key)) {
                throw new MissingParameterException($key);
            }
        }
        return true;
    }
}