<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

use Phalcon\Di;
use Models\User;
use Models\Catalog;
use Models\Offer;
use Classes\Syslog;
use Classes\RequestParams;
use Classes\Exceptions\NotFoundException;
use Classes\Exceptions\AppException;
use Classes\MobileSSO;
use Classes\Helper;
use Classes\PAPI\Requests\BalanceRequest;
use Classes\PAPI\UserStatus;
use Classes\PAPI\Requests\ValidationRequest;
use Classes\PAPI\Requests\RegistrationRequest;
use Classes\PAPI\Requests\WriteOffsRequest;
use Classes\PAPI\Requests\HistoryRequest;
use Classes\PAPI\Requests\GetCertificatesRequest;
use Classes\PAPI\Requests\PurchaseCertificateRequest;
use Classes\PAPI\Requests\RejectParticipationRequest;

define('ANDROID_APP_VERSION', '27');
define('IOS_APP_VERSION', '1.10');
define('TEST_NUMBERS', [
    '79137695612', '79150911170', '79199915120',
    '79199915345', '79154304534', '79154304544',
    '79126003604', '79886666434', '79817437872',
    '79175692331']);


$app->before(function () use ($app) {
    $currentRoute = $app->router->getMatchedRoute()->getCompiledPattern();
    if (in_array($currentRoute, ['/', '/deep-link', '/black-hole', '/test-interface'])) {
        return;
    }
    if (!$app->request->isSecure()) {
        // throw new AppException(
        //   401,
        //   'Connection must be secure.',
        //   'Unauthorized');
    }
    RequestParams::set((array)$app->request->getJsonRawBody(true));

    if ($currentRoute == '/sso/oauth2/callback') {
        return;
    }
    $os = $app->request->getHeader('OS');
    $version = $app->request->getHeader('App-Version');
    $androidVersionIsGood = $os == 'android' && Helper::compareVersion($version, ANDROID_APP_VERSION);
    $iosVersionIsGood = $os == 'ios' && Helper::compareVersion($version, IOS_APP_VERSION);
    if (!($androidVersionIsGood || $iosVersionIsGood)) {
        throw new AppException(
            400,
            'Пожалуйста, обновите приложение. Текущая версия больше не поддерживается.',
            'Old App-Version',
            ['Code' => 403]);
    }

});


$app->get('/', function () {
    return ['api v1.1'];
});


$app->get('/deep-link', function () use ($app) {
    $app->response->setContent(file_get_contents(APP_PATH . '/views/deep-link.html'));
    $app->response->send();
});


$app->get('/mobile-sso', function () use ($app) {
    return ['Link' => MobileSSO::getStartAuthLink()];
});


$app->get('/sso/oauth2/callback', function () use ($app) {
    $token = MobileSSO::finishAuth($app->request->getQuery());
    ValidationRequest::getResult(User::current());
    $app->response->setHeader('Authorization', $token->value());
    return ['Authorization successfully completed.'];
});


$app->get('/register', function () use ($app) {
    $user = User::current();
    $balance = BalanceRequest::getResult($user);
    $registrationNeeded = $balance->UserStatus != UserStatus::REGISTERED || $balance->UserStatus != UserStatus::IN_PROGRESS;
    if ($registrationNeeded === true) {
        $validation = ValidationRequest::getResult($user);
        // Валидация вернет null если абонент уже зарегистрирован
        if ($validation !== null) {
            // Регистрируем абоненета
            RegistrationRequest::getResult($user);
        }
        // Обновляем статус абонента
        BalanceRequest::getResult($user);
    }
    return;
});


$app->get('/profile', function () use ($app) {
    $user = User::current();
    $balance = BalanceRequest::getResult($user);
    return [
        'Msisdn' => $user->getMsisdn()->value(),
        'Status' => $balance->UserStatus,
        'AccountType' => $user->IsIndividual() ? 'b2c' : 'b2b',
        'TerminalId' => $user->getTerminalId()
    ];
});


$app->get('/balance', function () use ($app) {
    $user = User::current();
    $balance = BalanceRequest::getResult($user);
    $balance->registrationCheck();
    return ['Balance' => Helper::formatMoney($balance->BalanceValue)];
});


/*  Каталог предложений
 *  + /catalog -  Дерево из категорий/подкатегорий без предложений
 *  + /catalog?Section=<SectionId> - Массив предложений для запрошенной категории
 *  + /catalog?full - Полное дерево каталога
 */
$app->get('/catalog', function () use ($app) {
    if ($app->request->hasQuery('Section')) {
        $sectionId = (int)$app->request->getQuery('Section');
        return Offer::getAllForSection($sectionId);
    }

    if ($app->request->hasQuery('full')) {
        return Catalog::getFullTree();
    }

    if ($app->request->hasQuery('Search')) {
        $searchString = mb_ereg_replace("/[^A-Za-zА-Яа-я ]/", '', $app->request->getQuery('Search'));
        return Offer::searchFor($searchString);
    }

    return Catalog::getTree();
});


$app->get('/catalog_new', function () use ($app) {
    if ($app->request->hasQuery('Section')) {
        $sectionId = (int)$app->request->getQuery('Section');
        return Offer::getAllForSection($sectionId);
    }

    if ($app->request->hasQuery('full')) {
        return Catalog::getFullTree_new();
    }

    if ($app->request->hasQuery('Search')) {
        $searchString = mb_ereg_replace("/[^A-Za-zА-Яа-я ]/", '', $app->request->getQuery('Search'));
        return Offer::searchFor($searchString);
    }

    return Catalog::getTree();
});


$app->get('/history', function () use ($app) {
    $user = User::current();
    $balance = BalanceRequest::getResult($user);
    $balance->registrationCheck();

    $history = HistoryRequest::getResult($user);
    return $history->toArray();
});


$app->get('/write-offs', function () use ($app) {
    $user = User::current();
    $balance = BalanceRequest::getResult($user);
    $balance->registrationCheck();

    $history = WriteOffsRequest::getResult($user);
    return $history->toArray();
});


$app->get('/banners', function () {
    $user = User::current();
    return \Models\Banner::all();
});


$app->post('/feedback', function () use ($app) {
    $user = User::current();
    RequestParams::containsKeys(['Email', 'Subject', 'Message']);
    return \Classes\Mail::send(RequestParams::get('Email'), RequestParams::get('Subject'), RequestParams::get('Message'));
});


$app->post('/black-hole', function () use ($app) {
    $result = [];
    if (true == $app->request->hasFiles()) {
        $upload_dir = BASE_PATH . '/public/aploat/';

        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0755);
        }
        foreach ($app->request->getUploadedFiles() as $file) {
            $file->moveTo($upload_dir . $file->getName());
            $result[] = 'Loaded ' . $file->getName();
        }
    } else {
        $result[] = 'No files';
    }
    return $result;
});


$app->get('/certificates', function () use ($app) {
    $user = User::current();
    $balance = BalanceRequest::getResult($user)->BalanceValue;
    $prices = GetCertificatesRequest::getResult($user);
    $availablePrices = array_filter($prices, function ($price) use ($balance) {
        return $balance >= $price;
    });
    $availablePrice = empty($availablePrices) ? NULL : max($availablePrices);
    $result = [
        'prices' => $prices,
        'availablePrice' => $availablePrice
    ];
    if ($app->request->hasQuery('countRemainder')) {
        $result['remainder'] = empty($availablePrice) ? $balance : $balance - $availablePrice;
    }
    if ($app->request->hasQuery('nextQuarterEndDate')) {
        $result['expiryDate'] = Helper::nextQuarterEndDate();
    }
    return $result;
});


$app->post('/buy-certificate', function () use ($app) {
    $user = User::current();
    RequestParams::containsKeys(['Price']);
    $price = RequestParams::get('Price');
    return PurchaseCertificateRequest::getResult($user, ['price' => $price]);
});


$app->post('/reject-participation', function () use ($app) {
    $user = User::current();
    $response = RejectParticipationRequest::getResult($user);
    return ['result' => $response];
});


$app->get('/monitor', function () use ($app) {
    $config = Di::getDefault()->getConfig();
    $services = [
        'PAPI' => [$config->APP->domain, 80],
        'DB' => [$config->dbFreecom->host, 3306],
        'Queue' => [$config->rabbit->host, $config->rabbit->port],
        'Cache' => [$config->redis->host, $config->redis->port]
    ];
    $result = [];
    foreach ($services as $sName => $sSettings) {
        $sAddress = $sSettings[0];
        $service = [
            'service' => $sName,
            'address' => $sAddress
        ];
        $service['pingTime'] = Helper::pingHost($sAddress, $sSettings[1]);
        $result[] = $service;
    }
    return $result;
});


$app->notFound(function () use ($app) {
    throw new NotFoundException('API method not found');
});


$app->after(function () use ($app) {
    $app->response->setHeader('Content-Type', 'application/json; charset=UTF-8');
    $query = $app->request->getQuery();
    Syslog::write('api_response', json_encode([
        'request' => [
            'query' => $query,
            'headers' => $app->request->getHeaders(),
            'body' => $app->request->getRawBody()
        ],
        'response' => [
            'headers' => $app->response->getHeaders(),
            'body' => array_key_exists('_url', $query) ? ($query['_url'] == '/catalog' ? 'catalog' : $app->getReturnedValue()) : []
        ]
    ]));
    if ($app->getReturnedValue() === null) {
        return;
    }
    $app->response->setStatusCode(200);
    $app->response->setJsonContent($app->getReturnedValue());
    $app->response->send();
});


$app->error(function (Exception $error) use ($app) {
    Syslog::write('api_response', json_encode([
        'request' => [
            'query' => $app->request->getQuery(),
            'headers' => $app->request->getHeaders(),
            'body' => $app->request->getRawBody()
        ],
        'error' => [
            'exceptionType' => get_class($error),
            'code' => $error->getCode(),
            'message' => $error->getMessage(),
            'trace' => $error->getTraceAsString()
        ]
    ]));
    if ($error instanceof AppException) {
        $app->response->setStatusCode($error->getCode(), $error->getTitle());
        $content['Message'] = $error->getMessage();
        if (!empty($error->getContent())) {
            $content['Details'] = $error->getContent();
        }
        $app->response->setJsonContent($content);
    } else {
        $app->response->setStatusCode($error->getCode(), $error->getMessage());
    }
    $app->response->send();
});
