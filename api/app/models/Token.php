<?php

namespace Models;

use Classes\Exceptions\ValueException;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model;
use Phalcon\Security\Random;

class Token extends Model
{
    protected $TokenId;
    protected $Token;
    protected $UserId;
    protected $Expires;
    private $_user;

    const TEMP_TOKEN_EXPIRATION_TIME = '1 minute';
    const TOKEN_EXPIRATION_TIME = '1 year';

    /** @var  Token Token won't change in current request. */
    private static $_current;

    /**
     * Trying to create token from request and then checking if it exists.
     * @return Token
     * @throws ValueException
     */
    public static function fromRequest()
    {
        if (static::$_current != null) {
            return static::$_current;
        }
        $request = new Request();
        $stringToken = $request->getHeader('Authorization');
        if (!empty($stringToken)) {
            /* TODO: format check */
            /** @var Token $token */
            $token = Token::findFirst([
                'Token=?0',
                'bind' => [$stringToken]
            ]);
            if ($token != null) {
                if ($token->Expires > date(DATE_FORMAT)) {
                    $token->_user = User::findFirst([
                        'UserId=?0',
                        'bind' => [$token->UserId]
                    ]);
                    static::$_current = $token;
                    return $token;
                }
                $token->delete();
                throw new ValueException('Token expired.');
            }
        }
        throw new ValueException('Bad token');
    }

    /**
     * Token value.
     * @return string
     */
    public function value() : string
    {
        return $this->Token;
    }

    /**
     * User.
     * @return User
     */
    public function getUser() : User
    {
        return $this->_user;
    }

    public static function fromString(User $user, string $token)
    {
        return static::_generate($user, static::TOKEN_EXPIRATION_TIME, $token);
    }

    /**
     * Generate token for user for 1 year.
     * @param User $user
     * @return Token
     */
    public static function generate(User $user) : Token
    {
        return static::_generate($user, static::TOKEN_EXPIRATION_TIME);
    }

    /**
     * Generate token for user with expiration time.
     * @param User $user
     * @param string $expires
     * @param string $defaultToken
     * @return Token
     */
    private static function _generate(User $user, string $expires, string $defaultToken = '') : Token
    {
        $token = new Token();
        $token->UserId = $user->getUserId();
        $token->Expires = date(DATE_FORMAT, strtotime($expires));
        if (empty($defaultToken)) {
            $random = new Random();
            $token->Token = $random->uuid();
        } else {
            $token->Token = $defaultToken;
        }
        $token->save();
        return $token;
    }

    public function getSource()
    {
        return 'Token';
    }

}