<?php
namespace Models;


use Phalcon\Db\Adapter\Pdo;
use Phalcon\Di;

class BaseModel
{
    /** @var  Pdo */
    protected static $_connection;

    public static function connection() : Pdo
    {
        if (static::$_connection == null) {
            static::$_connection = Di::getDefault()->getShared('db');
        }
        return static::$_connection;
    }

}