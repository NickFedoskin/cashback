<?php
namespace Models;


use Phalcon\Cache\Backend\Redis;
use Phalcon\Di;

class Catalog extends BaseModel
{
    const REDIS_KEY = 'catalog';

    public static function getTree() : array
    {
        /** @var Redis $redis */
        $redis = Di::getDefault()->getShared('redis');
        if ($redis->exists(static::REDIS_KEY)) {
            $tree = $redis->get(static::REDIS_KEY);
        } else {
            $tree = static::getFromDB();
        }
        return Offer::mapEncryptedMsisdn($tree);
    }

    public static function getFromDB() : array
    {
        $baseUrl = Di::getDefault()->getShared('baseUrl');
        $query = <<<SQL
SELECT SectionId, Name, CONCAT('$baseUrl', Image) as Image, ParentSectionId, CONCAT('$baseUrl', Icon) as Icon
FROM Section ORDER by Priority
SQL;

        $arr = static::connection()->fetchAll($query);

        $new = array();
        foreach ($arr as $section) {
            $offers = Offer::getAllFromDB($section['SectionId']);
            $offersCount = count($offers);
            if ($offersCount == 0) {
                continue;
            } elseif ($offersCount == 1) {
                $bestOffer = array_shift($offers);
                $section['UrlTemplate'] = $bestOffer['UrlTemplate'];
            }
            $section['OffersCount'] = (string)$offersCount;
            $parent = $section['ParentSectionId'];
            if ($section['ParentSectionId'] == 0) {
                $section['BestValue'] = Offer::getBestValueForSection($section['SectionId']);
                unset($section['ParentSectionId']);
            } else {
                $section['Caption'] = static::pluralForm(
                    $section['OffersCount'],
                    array('предложение', 'предложения', 'предложений')
                );
            }
            $new[$parent][] = $section;
        }
        $tree = [];
        if (count($new) > 0) {
            $root = $new[0];
            foreach ($root as $item) {
                if (array_key_exists($item['SectionId'], $new)) {
                    $item['SubSections'] = $new[$item['SectionId']];
                }
                $tree[] = $item;
            }
        }
        return $tree;
    }

    protected static function pluralForm($number, $after)
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    public static function getFullTree()
    {
        $tree = static::getTree();
        foreach ($tree as &$section) {
            if ($section['SectionId'] == 1) {
                foreach ($section['SubSections'] as &$subSection) {
                    $subSection['Offers'] = Offer::getAllForSection($subSection['SectionId']);
                }
            } else {
                $section['Offers'] = Offer::getAllForSection($section['SectionId']);
            }
        }
        return $tree;
    }


    public static function getFullTree_new()
    {
        $tree = static::getTree();
        foreach ($tree as &$section) {
            if ($section['SectionId'] == 1) {
                foreach ($section['SubSections'] as &$subSection) {
                    $subSection['Offers'] = Offer::getAllForSection_new($subSection['SectionId']);
                }
            } else {
                $section['Offers'] = Offer::getAllForSection_new($section['SectionId']);
            }
        }
        return $tree;
    }
}