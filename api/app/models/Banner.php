<?php


namespace Models;


use Phalcon\Di;
use Phalcon\Mvc\Model;

class Banner extends Model
{

    public $Image;
    public $Title;
    public $Description;
    public $ButtonCaption;
    public $ButtonRefType;
    public $ButtonRef;
    public $Hidden;

    public static function all()
    {
        $result = [];
        $baseUrl = Di::getDefault()->getShared('baseUrl');
        $msisdn = urlencode(User::current()->getEncryptedMsisdn());
        /** @var Banner[] $banners */
        $banners = Banner::find(['Hidden=false']);
        foreach ($banners as $banner) {
            $result[] = [
                'Image' => $baseUrl . $banner->Image,
                'Title' => $banner->Title,
                'Desc' => $banner->Description,
                'Button' => [
                    'Caption' => $banner->ButtonCaption,
                    'RefType' => $banner->ButtonRefType,
                    'Ref' => str_replace('(subsid)', $msisdn, $banner->ButtonRef)
                ]
            ];
        }
        return $result;
    }

    public function getSource()
    {
        return 'Banner';
    }

}