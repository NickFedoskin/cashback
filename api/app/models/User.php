<?php
/**
 * Created by PhpStorm.
 * User: avaylin
 * Date: 05.06.2017
 * Time: 15:01
 */

namespace Models;

use Classes\MSISDN;
use Phalcon\Mvc\Model;

class User extends Model
{
    #region Fields
    protected $UserId;
    /** @var  string */
    protected $Msisdn;
    /** @var boolean */
    protected $Individual;
    /** @var int */
    protected $TerminalId;
    #endregion

    /** @var User */
    private static $_current;

    public static function current() : User
    {
        if (static::$_current == null) {
            static::$_current = Token::fromRequest()->getUser();
        }
        return static::$_current;
    }

    public static function getUserByMSISDN(MSISDN $msisdn) : User
    {
        $user = User::findFirst([
            'Msisdn=?0',
            'bind' => [$msisdn->value()]
        ]);
        if ($user === false) {
            $user = new User();
            $user->Msisdn = $msisdn->value();
            $user->save();
        }
        return static::$_current = $user;
    }


    public function getUserId() : int
    {
        return $this->UserId;
    }

    public function getMsisdn() : MSISDN
    {
        return new MSISDN($this->Msisdn);
    }

    private $_encryptedMsisdn;

    public function getEncryptedMsisdn() : string
    {
        if (!isset($this->_encryptedMsisdn)) {
            $this->_encryptedMsisdn = $this->getMsisdn()->encrypt();
        }
        return $this->_encryptedMsisdn;
    }

    public function getSource()
    {
        return 'User';
    }

    public function setIsIndividual(bool $value)
    {
        $this->Individual = $value;
    }

    public function IsIndividual() : bool
    {
        return $this->Individual;
    }

    public function setTerminalId(int $terminalId)
    {
        $this->TerminalId = $terminalId;
    }

    public function getTerminalId() : int
    {
        return $this->TerminalId;
    }

}