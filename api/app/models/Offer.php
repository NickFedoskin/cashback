<?php
namespace Models;


use Phalcon\Cache\Backend\Redis;
use Phalcon\Db;
use Phalcon\Di;

class Offer extends BaseModel
{

    const MSISDN_STR_TEMPLATE = '(subsid)';
    const CHANNEL_TEMPLATE = '(channelid)';
    const REDIS_KEY_TEMPLATE = 'offers-for-section-';
    const REDIS_KEY_ALL_OFFERS = 'all-offers';

    protected static function getBestValueForOffer(int $offerId)
    {
        $currentDate = date(DATE_FORMAT);
        $query = <<<SQL
SELECT cm.Value, cm.Code 
FROM CalculationMethod cm, Product p
WHERE 
p.OfferId = :OfferId 
AND p.ProductId = cm.ProductId
AND (p.DateFrom <= '$currentDate' || p.DateFrom is null)
AND (p.DateTo >= '$currentDate' || p.DateTo is null)
ORDER BY cm.Code DESC, Value DESC
SQL;
        $products = static::connection()->fetchAll(
            $query,
            Db::FETCH_ASSOC,
            ['OfferId' => $offerId]);
        if (empty($products)) {
            return null;
        }
        $prefix = '';
        if (count($products) > 1) {
            $prefix = 'до';
        }
        return array_merge(['Prefix' => $prefix], array_shift($products));
    }

    public static function getBestValueForSection(int $sectionId)
    {
        $currentDate = date(DATE_FORMAT);
        $query = <<<SQL
SELECT cm.Value, cm.Code, o.OfferId
FROM CalculationMethod cm, Product p, OfferSection os, Partner pr, Offer o, DisplayOption do
WHERE 
os.SectionId = :SectionId
AND os.Type = 1
AND o.OfferId = os.OfferId
AND o.OfferId = p.OfferId 
AND p.ProductId = cm.ProductId
AND o.PartnerId = pr.PartnerId
AND do.OfferId = o.OfferId

AND (p.DateFrom <= '$currentDate' || p.DateFrom is null)
AND (p.DateTo >= '$currentDate' || p.DateTo is null)
AND (o.DateFrom <= '$currentDate' || o.DateFrom is null)
AND (o.DateTo >= '$currentDate' || o.DateTo is null)
AND (pr.DateFrom <= '$currentDate' || pr.DateFrom is null)
AND (pr.DateTo >= '$currentDate' || pr.DateTo is null)

AND do.Type='App' AND do.Enable=true 
ORDER BY cm.Code DESC, Value DESC
SQL;
        $bestValues = static::connection()->fetchAll(
            $query,
            Db::FETCH_ASSOC,
            ['SectionId' => $sectionId]);
        if ($bestValues === false) {
            return null;
        }

        $bestValue = $bestValues[0];
        $bestValue['Prefix'] = '';
        $lastValue = $bestValues[count($bestValues) - 1];

        // Если значения предложений разные, нужно об этом сообщить добавив префикс "до"
        if ($bestValue['Value'] != $lastValue['Value']) {
            $bestValue['Prefix'] = 'до';
        }
        unset($bestValue['OfferId']);
        return $bestValue;
    }

    public static function getCountForSection(int $sectionId)
    {
        return count(static::getAllForSection($sectionId));
    }

    public static function getAllForSection(int $sectionId)
    {
        /** @var Redis $redis */
        $redis = Di::getDefault()->getShared('redis');
        $key = static::REDIS_KEY_TEMPLATE . $sectionId;
        if ($redis->exists($key)) {
            $offers = $redis->get($key);
        } else {
            $offers = static::getAllFromDB($sectionId);
        }
        return static::mapEncryptedMsisdn($offers);
    }

    public static function getAllFromDB(int $sectionId)
    {
        $baseUrl = Di::getDefault()->getShared('baseUrl');
        $currentDate = date(DATE_FORMAT);
        $query = <<<SQL
SELECT o.OfferId, pr.Description as Name, o.UrlTemplate,
case 
  when length(pr.LogoRef) = 0 or pr.LogoRef is null 
  then CONCAT('$baseUrl', pr.Logo) 
  else pr.LogoRef 
end  as Logo,
HashId, pr.Name as PartnerName
FROM 
OfferSection os, Offer o, Partner pr, DisplayOption do
WHERE o.OfferId = os.OfferId
AND o.PartnerId = pr.PartnerId
AND do.OfferId = o.OfferId
AND os.Type = 1
AND os.SectionId = $sectionId

AND (o.DateFrom <= '$currentDate' || o.DateFrom is null)
AND (o.DateTo >= '$currentDate' || o.DateTo is null)
AND (pr.DateFrom <= '$currentDate' || pr.DateFrom is null)
AND (pr.DateTo >= '$currentDate' || pr.DateTo is null)

AND do.Type='App' AND do.Enable=true
ORDER BY do.Priority DESC 
SQL;
        $offers = static::connection()->fetchAll($query);
        foreach ($offers as $key => &$offer) {
            $offer['BestOffer'] = static::getBestValueForOffer($offer['OfferId']);
            if ($offer['BestOffer'] == null) {
                unset($offers[$key]);
                continue;
            }
            if (empty($offer['Name'])) {
                $offer['Name'] = 'Предложение';
            }

            $offer['UrlTemplate'] = str_ireplace(static::CHANNEL_TEMPLATE, 'app', $offer['UrlTemplate']);

            $offer['MarketingCampaign'] = null;
            $maCampaignQuery = <<<SQL
SELECT Id, Name
FROM MarketingCampaign
WHERE OfferId = {$offer['OfferId']}
AND DateFrom <= '$currentDate'
AND DateTo >= '$currentDate'
SQL;
            $marketingCampaign = static::connection()->fetchOne($maCampaignQuery);
            if ($marketingCampaign !== false) {
                $textColor = '#FFFFFF';
                switch ($marketingCampaign['Id']) {
//          case 1:
//            $color = '#F44336';
//            break;
//          case 2:
//            $color = '#F5A623';
//            break;
                    default:
                        $color = '#F44336';
                }
                $offer['MarketingCampaign'] = array_merge($marketingCampaign, ['Background' => $color, 'TextColor' => $textColor]);
            }

            $offer['CashBackDescription'] = null;
            $descriptionQuery = <<<SQL
SELECT DelayUnit, MetricUnitNumber
FROM CashBackDescription
WHERE OfferId = {$offer['OfferId']}
SQL;
            $cashBackDesc = static::connection()->fetchOne($descriptionQuery);
            if ($cashBackDesc !== false) {
                $delay = $cashBackDesc['DelayUnit'];
                $caption = 'дней';
                if ($delay == 1) {
                    $caption = 'день';
                } else if (1 < $delay && $delay < 5) {
                    $caption = 'дня';
                }
                $offer['CashBackDescription']['DelayUnit'] = $delay . ' ' . $caption;
            }

            unset($offer['OfferId']);
        }
        return array_values($offers);
    }

    public static function mapEncryptedMsisdn($array, $urlKey = 'UrlTemplate') : array
    {
        $encMsisdn = User::current()->getEncryptedMsisdn();
        array_walk_recursive($array, function (&$elem, $key, $params) {

            if ($key == $params['urlKey']) {
                $elem = str_ireplace(static::MSISDN_STR_TEMPLATE, $params['encMsisdn'], $elem);
            }
        }, ['urlKey' => $urlKey, 'encMsisdn' => $encMsisdn]);
        return $array;
    }

    public static function getAllOffers() : array
    {
        $currentDate = date(DATE_FORMAT);
        $query = <<<SQL
SELECT HashId, pr.Name as PartnerName
FROM 
OfferSection os, Offer o, Partner pr, DisplayOption do
WHERE o.OfferId = os.OfferId
AND o.PartnerId = pr.PartnerId
AND do.OfferId = o.OfferId
AND os.Type = 1

AND (o.DateFrom <= '$currentDate' || o.DateFrom is null)
AND (o.DateTo >= '$currentDate' || o.DateTo is null)
AND (pr.DateFrom <= '$currentDate' || pr.DateFrom is null)
AND (pr.DateTo >= '$currentDate' || pr.DateTo is null)

AND do.Type='App' AND do.Enable=true
GROUP BY HashId, PartnerName
SQL;
        $offers = static::connection()->fetchAll($query);
        if ($offers === false) {
            $offers = [];
        }
        return $offers;
    }

    public static function searchFor(string $searchString) : array
    {
        if (empty($searchString))
            return [];
        /** @var Redis $redis */
        $redis = Di::getDefault()->getShared('redis');
        $key = static::REDIS_KEY_ALL_OFFERS;
        if ($redis->exists($key)) {
            $offers = $redis->get($key);
        } else {
            $offers = static::getAllOffers();
        }
        $searchResult = [];
        foreach ($offers as $offer) {
            if (mb_stripos($offer['PartnerName'], $searchString) !== false) {
                $searchResult[] = $offer['HashId'];
            }
        }
        return $searchResult;
    }


    public static function getAllForSection_new(int $sectionId)
    {
        /** @var Redis $redis */
        $redis = Di::getDefault()->getShared('redis');
        $key = static::REDIS_KEY_TEMPLATE . $sectionId;
        if ($redis->exists($key)) {
            $offers = $redis->get($key);
        } else {
            $offers = static::getAllFromDB_new($sectionId);
        }
        return static::mapEncryptedMsisdn($offers);
    }

    public static function getAllFromDB_new($sectionId)
    {
        $baseUrl = Di::getDefault()->getShared('baseUrl');
        $currentDate = date(DATE_FORMAT);
        $query = <<<SQL
SELECT o.OfferId, pr.Description as Name, o.UrlTemplate,
case 
  when length(pr.LogoRef) = 0 or pr.LogoRef is null 
  then CONCAT('$baseUrl', pr.Logo) 
  else pr.LogoRef 
end  as Logo,
HashId, pr.Name as PartnerName
FROM 
OfferSection os, Offer o, Partner pr, DisplayOption do
WHERE o.OfferId = os.OfferId
AND o.PartnerId = pr.PartnerId
AND do.OfferId = o.OfferId
AND os.Type = 1
AND os.SectionId = $sectionId

AND (o.DateFrom <= '$currentDate' || o.DateFrom is null)
AND (o.DateTo >= '$currentDate' || o.DateTo is null)
AND (pr.DateFrom <= '$currentDate' || pr.DateFrom is null)
AND (pr.DateTo >= '$currentDate' || pr.DateTo is null)

AND do.Type='App' AND do.Enable=true
ORDER BY do.Priority DESC 
SQL;
        $offers = static::connection()->fetchAll($query);
        foreach ($offers as $key => &$offer) {
            $offer['BestOffer'] = static::getBestValueForOffer($offer['OfferId']);
            if ($offer['BestOffer'] == null) {
                unset($offers[$key]);
                continue;
            }
            if (empty($offer['Name'])) {
                $offer['Name'] = 'Предложение';
            }

            $offer['UrlTemplate'] = str_ireplace(static::CHANNEL_TEMPLATE, 'app', $offer['UrlTemplate']);

            $offer['MarketingCampaign'] = null;
            $maCampaignQuery = <<<SQL
SELECT Id, Name
FROM MarketingCampaign
WHERE OfferId = {$offer['OfferId']}
AND DateFrom <= '$currentDate'
AND DateTo >= '$currentDate'
SQL;
            $marketingCampaign = static::connection()->fetchOne($maCampaignQuery);
            if ($marketingCampaign !== false) {
                $textColor = '#FFFFFF';
                switch ($marketingCampaign['Id']) {
//          case 1:
//            $color = '#F44336';
//            break;
//          case 2:
//            $color = '#F5A623';
//            break;
                    default:
                        $color = '#F44336';
                }
                $offer['MarketingCampaign'] = array_merge($marketingCampaign, ['Background' => $color, 'TextColor' => $textColor]);
            }

            $offer['CashBackDescription'] = null;
            $descriptionQuery = <<<SQL
SELECT DelayUnit, MetricUnitNumber
FROM CashBackDescription
WHERE OfferId = {$offer['OfferId']}
SQL;
            $cashBackDesc = static::connection()->fetchOne($descriptionQuery);
            if ($cashBackDesc !== false) {
                $delay = $cashBackDesc['DelayUnit'];
                $caption = 'дней';
                if ($delay == 1) {
                    $caption = 'день';
                } else if (1 < $delay && $delay < 5) {
                    $caption = 'дня';
                }
                $offer['CashBackDescription']['DelayUnit'] = $delay . ' ' . $caption;
            }

            unset($offer['OfferId']);
        }
        return array_values($offers);
    }

}